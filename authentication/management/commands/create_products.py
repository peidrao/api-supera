import json
from django.core.management.base import BaseCommand

from product.models import Product


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('./products.json', 'r') as f:
            json_file = json.load(f) 
            for product in json_file:
                if not Product.objects.filter(name=product.get('name')).exists():
                    Product.objects.create(
                            name=product['name'],
                            price=product['price'],
                            score=product['score'],
                            image=product['image']
                    )