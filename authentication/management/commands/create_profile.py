from django.core.management.base import BaseCommand

from authentication.models import Profile


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not Profile.objects.filter(email="user@user.com.br").exists():
            profile = Profile.objects.create(
                email="user@user.com.br", 
                username="User", 
                first_name="User",
                last_name="Test",
                )
            profile.set_password('123')
            profile.save()
