import jwt
from rest_framework import views, exceptions, status, generics
from rest_framework.response import Response
from django.conf import settings


from authentication.models import Profile
from authentication.serializers import ProfileMeSerializer, ProfileSerializer


class ProfileCreateView(generics.CreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class TokenIsValidView(views.APIView):
    def get(self, request):
        return Response(request.data)

    def post(self, request, *args, **kwargs):
        try:
            token = request.data["access"]
            decode = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        except jwt.ExpiredSignatureError:
            response = Response(
                status=status.HTTP_403_FORBIDDEN,
                data={'error': 'access_token expired'},
            )
            return response
        except jwt.InvalidSignatureError:
            raise exceptions.AuthenticationFailed('access_token invalid token')
        except KeyError:
            raise exceptions.AuthenticationFailed('access_token invalid key')
    
        profile = Profile.objects.filter(is_active=True).get(id=decode["user_id"])
        serializer = ProfileMeSerializer(profile)
        
        return Response(serializer.data)