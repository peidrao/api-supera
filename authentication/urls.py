from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView
)

from authentication.views import TokenIsValidView, ProfileCreateView

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token_is_valid/', TokenIsValidView.as_view(), name='token_is_valid'),
    path('user/', ProfileCreateView.as_view(), name='profile_create'),
]
