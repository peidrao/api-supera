from django.db import models
from django.contrib.auth.models import AbstractUser


class Profile(AbstractUser):
    email = models.CharField(max_length=133, unique=True)
    phone = models.CharField(max_length=255, null=True, blank=True)


    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
