from rest_framework import serializers
from authentication.models import Profile


class ProfileMeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'email', 'first_name', 'last_name', 'username')


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'email', 'first_name', 'last_name', 'password', 'username')
        
    
    def create(self, validated_data):
        password = validated_data.pop('password')
        profile = super(ProfileSerializer, self).create(validated_data)   

        profile.set_password(password)
        profile.save()
        
        return profile