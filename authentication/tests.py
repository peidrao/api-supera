from django.test import TestCase, Client
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate

from  authentication.models import Profile
from  authentication.views import ProfileCreateView
from rest_framework_simplejwt.views import TokenObtainPairView


class ProfileCreateViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.url = 'user/'

    def test_create_profile(self):
        view = ProfileCreateView.as_view()
        payload = {
        	"email": "purdy@gmail.com",
	        "username": "purdy",
        	"first_name": "Purdy",
	        "password": "123"
        }
        request = self.factory.post(self.url, payload, format='json')
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Profile.objects.filter(email='purdy@gmail.com').exists())

    def test_create_profile_not_payload(self):
        view = ProfileCreateView.as_view()
        request = self.factory.post(self.url, {}, format='json')
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_profile_not_password(self):
        view = ProfileCreateView.as_view()
        payload ={	
            "email": "purdy@gmail.com",
	        "username": "purdy",
        	"first_name": "Purdy",
        }
        request = self.factory.post(self.url, payload, format='json')
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_profile_already_exists(self):
        view = ProfileCreateView.as_view()
        Profile.objects.get_or_create(email='purdy@gmail.com')[0]
        payload = {
        	"email": "purdy@gmail.com",
	        "username": "purdy",
        	"first_name": "Purdy",
	        "password": "123"
        }
        request = self.factory.post(self.url, payload, format='json')
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['email'][0].title(), 'User With This Email Already Exists.')


class TokenObtainPairViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.url = 'token/'
        profile = Profile.objects.get_or_create(email='purdy@gmail.com', is_active=True)[0]
        profile.set_password('123')
        profile.save()

    def test_login_profile(self):
        view = TokenObtainPairView.as_view()
        payload = {
        	"email": "purdy@gmail.com",
	        "password": "123"
        }
        request = self.factory.post(self.url, payload, format='json')
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('access', response.data)
        self.assertIn('refresh', response.data)
    
    def test_login_profile_not_payload(self):
        view = TokenObtainPairView.as_view()
        request = self.factory.post(self.url, {}, format='json')
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
