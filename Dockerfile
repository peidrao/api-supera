FROM python:3.9

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /supera

ADD requirements.txt /supera/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ADD . /supera/