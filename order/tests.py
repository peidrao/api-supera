from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from order.models import Order, OrderItem
from order.utils import generate_code
from order.views import OrderCreateView
from authentication.models import Profile
from product.models import Product


class GenerateCodeTestCase(TestCase):

    def test_create_code(self):
        code = generate_code()       

        self.assertEqual(len(code), 10)


class OrderCreateViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.profile = Profile.objects.get_or_create(email='profile@email.com', password='123')[0]
        cls.product1 = Product.objects.get_or_create(name='P1', price=500, score=100)[0]
        cls.product2 = Product.objects.get_or_create(name='P2', price=500, score=100)[0]
        cls.product3 = Product.objects.get_or_create(name='P3', price=200, score=100)[0]
        cls.url = 'orders/'

    def test_create_order(self):
        view = OrderCreateView.as_view()
        
        payload = {
            'tax' : 20,
            'price': 1200,
            'products': [self.product1.id, self.product2.id, self.product3.id]
        }

        request = self.factory.post(self.url, payload, format='json')
        force_authenticate(request, user=self.profile)
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['message'], 'Pedido criado com sucesso')
        self.assertEqual(Order.objects.all().count(), 1)
        self.assertEqual(OrderItem.objects.all().count(), 3)
    
    def test_list_orders(self):
        view = OrderCreateView.as_view()
        
        order = Order.objects.create(code='#12code#1', total=100, tax=12, profile=self.profile)
        OrderItem.objects.create(order=order, product=self.product1, profile=self.profile)
        OrderItem.objects.create(order=order, product=self.product2, profile=self.profile)

        request = self.factory.get(self.url)
        force_authenticate(request, user=self.profile)
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['status_order'], 'Open')
        self.assertEqual(response.data[0]['quantity'], 2)
