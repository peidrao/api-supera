from rest_framework import serializers
from order.models import Order


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = "__all__"


class OrderListSerializer(serializers.ModelSerializer):
    quantity = serializers.SerializerMethodField()
    status_order = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = "__all__"

    def get_status_order(self, obj):
        return obj.get_status_display()

    def get_quantity(self, obj):
        return obj.orderitem_set.all().count()
