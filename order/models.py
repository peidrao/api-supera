from django.db import models
from django.utils import timezone
from authentication.models import Profile
from product.models import Product

class Order(models.Model):
    class StatusChoice(models.IntegerChoices):
        OPEN = 1, 'Open'
        CLOSED = 2, 'Closed'

    code = models.CharField(max_length=40)
    total = models.FloatField()
    tax = models.FloatField()
    created_at = models.DateTimeField(default=timezone.now)
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    status = models.IntegerField(default=StatusChoice.OPEN, choices=StatusChoice.choices)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    create_at = models.DateTimeField(default=timezone.now)
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
