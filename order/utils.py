import secrets
import string

def generate_code():
    alphabet = string.ascii_letters + string.digits
    code = ''.join(secrets.choice(alphabet) for _ in range(10))
    return code
