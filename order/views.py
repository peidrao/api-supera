from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from order.utils import generate_code

from product.models import Product
from order.models import Order, OrderItem
from order.serializers import OrderSerializer, OrderListSerializer


class OrderCreateView(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        tax = request.data.get('tax')
        price = request.data.get('price')
        products = request.data.get('products')
        products = Product.objects.filter(id__in=products)
        code = generate_code()
        order = self.queryset.create(code=code, total=price, tax=tax, profile=request.user)

        for p in products:
            OrderItem.objects.create(product=p, profile=request.user, order=order)
        
        return Response({'message': 'Pedido criado com sucesso'}, status=status.HTTP_201_CREATED)
    
    def list(self, request):
        orders = self.queryset.filter(profile=request.user)

        serializer = OrderListSerializer(orders, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
