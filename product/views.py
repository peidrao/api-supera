from rest_framework import generics

from product.models import Product
from product.serializers import  ProductSerializer


class ProductListView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_queryset(self):
        queryset = self.queryset.all()
        q = self.request.query_params.get('q')
        ordering = self.request.query_params.get('ordering')

        if q:
            queryset = queryset.filter(name__icontains=q)

        if ordering:
            queryset = queryset.order_by(ordering)
            
        return queryset.distinct()