from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory

from  product.models import Product
from  product.views import ProductListView


class ProductListViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.url = 'products/'

        for index in range(1, 10):
            Product.objects.create(name=f'Product {index}', price=10, score=index, image=f'image-{index}.jpg' )

    def test_list_products(self):
        view = ProductListView.as_view()
        request = self.factory.get(self.url)
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 9)
    
    def test_list_products_q(self):
        view = ProductListView.as_view()
        Product.objects.create(name='Purdy', price=10, score=12)

        request = self.factory.get(self.url, {'q': 'Purdy'}, format='json')
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
    
    def test_list_products_ordering_asc(self):
        view = ProductListView.as_view()

        request = self.factory.get(self.url, {'ordering': 'score'}, format='json')
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['name'], 'Product 1')
        self.assertEqual(response.data[1]['name'], 'Product 2')
        self.assertEqual(response.data[2]['name'], 'Product 3')

    def test_list_products_ordering_des(self):
        view = ProductListView.as_view()

        request = self.factory.get(self.url, {'ordering': '-score'}, format='json')
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['name'], 'Product 9')
        self.assertEqual(response.data[1]['name'], 'Product 8')
        self.assertEqual(response.data[2]['name'], 'Product 7')
