from django.urls import path

from product.views import ProductListView

urlpatterns = [
    path('products/', ProductListView.as_view(), name='products_list'),    
]
