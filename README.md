# API SUPERA

## Como usar o projeto

O primeiro passo para fazer o _start_ do projeto é ter o docker e o docker-compose instalados em sua maquina. Tendo esses itens podemos criar o nosso projeto usando o

```console
purdy@49ers:~$ make run
```

ou também

```console
purdy@49ers:~$ docker-compose up -d
```

Após isso o docker ficara responsável por instalar todas as dependências do projeto, assim como também a criação do banco de dados.

```console
purdy@49ers:~$ make migrate
```

Podemos ver os logs da nossa aplicação com

```console
purdy@49ers:~$ make logs
```

Criando migrações no banco de dados.

## Preenchendo banco de dados

Para criar um novo usuário

```console
purdy@49ers:~$ make profile
```

E, para preencher o banco de dados com os produtos

```console
purdy@49ers:~$ make seed
```

## Testes unitários

Para todos os serviços feitos existem testes unitários. Optou-se em usar o próprio TestCase, biblioteca nativa do Django.

Para rodar os testes

```console
purdy@49ers:~$ make test
```

Para vermos a cobertura de testes que o nosso código tem, precisaremos rodas dois comandos em sequência

```console
purdy@49ers:~$ make coverage
```

e

```console
purdy@49ers:~$ make html
```

Será criado um diretório dentro do nosso projeto chamado **htmlcov**. Dentro dele existem vários arquivos html, e entre eles o **index.html**, basta abrir o mesmo no navegador e teremos a porcentagem mais arquivos que estão cobertos.

## Endpoints

- **Authentication**
- `POST /api/token/` - Obter o token a partir das credenciais do usuário
- `POST /api/refresh/` - Atualiza o token do usuário
- `POST /api/token_is_valid/` - Faz a validação do token e retorna algumas informações do usuário
- `POST /api/user/` - Criação de novos usuários
- **Product**
- `GET /api/products/` - Listagem e filtro de produtos.
- **Order**
- `GET/POST /api/orders/` - Criação e listagem de pedidos.

## Swagger

Foi adicionando o swagger como também o redoc. As rotas para os respectivos serviços são:

- `GET /api/swagger/`
- `GET /api/redoc/`
