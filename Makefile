run:
	docker-compose up -d

logs:
	docker attach api-web-1

make:
	docker exec -it api-web-1 python manage.py makemigrations

bash:
	docker exec -it api-web-1 bash

migrate:
	docker exec -it api-web-1 python manage.py migrate

test:
	docker exec -it api-web-1 python manage.py test --keepdb
	
profile:
	docker exec -it api-web-1 python manage.py create_profile

seed:
	docker exec -it api-web-1 python manage.py create_products

coverage:
	docker exec -it api-web-1 coverage run manage.py test --keepdb

html:
	docker exec -it api-web-1 coverage html